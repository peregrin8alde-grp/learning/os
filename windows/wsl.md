# WSL

https://docs.microsoft.com/ja-jp/windows/wsl/

管理者の PowerShell または Windows コマンド プロンプトに次のコマンドを入力し、コンピューターを再起動することによって、
Linux 用 Windows サブシステム (WSL) を実行するために必要なすべてをインストールできる。

```
wsl --install
```

## setup

https://docs.microsoft.com/ja-jp/windows/wsl/setup/environment

### Linux のユーザー名とパスワードを設定する

`sudo` 可能な初期ユーザ作成がインストール後の初回起動時に実施される。パスワードの変更を行う場合は以下。

```
wsl -u root

passwd <username>
```

### パッケージの更新とアップグレード

```
sudo apt update && sudo apt upgrade
```

### File Storage

- WSL プロジェクトを Windows エクスプローラーで開くには、次のコマンドを入力します。 `explorer.exe .` 
現在のディレクトリを開くために、必ずコマンドの最後にピリオドを追加します。
- https://docs.microsoft.com/ja-jp/windows/wsl/filesystems#file-storage-and-performance-across-file-systems
  Linux ツールを使用して Linux コマンド ライン (Ubuntu、OpenSUSE など) で作業している場合、
  最速のパフォーマンス速度を実現するには、ファイルを WSL ファイル システムに格納します。
   Windows ツールを使用して Windows コマンド ライン (PowerShell、コマンド プロンプト) で作業している場合、
   ファイルを Windows ファイル システムに格納します。 オペレーティングシステム全体のファイルにアクセスできますが、
   パフォーマンスが大幅に低下する可能性があります。
- たとえば、WSL プロジェクト ファイルを格納する場合は、次のとおりです。
  - Linux ファイル システムのルート ディレクトリを使用します: `\\wsl$\<DistroName>\home\<UserName>\Project`
  - Windows ファイル システムのルート ディレクトリは使用しません: `C:\Users\<UserName>\Project または /mnt/c/Users/<UserName>/Project$`

### お気に入りのコード エディターを設定する

#### Visual Studio Code の使用

https://docs.microsoft.com/ja-jp/windows/wsl/tutorials/wsl-vscode

### Git を使用してバージョン管理を設定する

https://docs.microsoft.com/ja-jp/windows/wsl/tutorials/wsl-git

### Docker を使用してリモート開発コンテナーを設定する

https://docs.microsoft.com/ja-jp/windows/wsl/tutorials/wsl-containers

通常の Ubuntu と同様に Docker Engine を使いたい場合は以下。

https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository

```
sudo apt-get update
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io

# WSL 起動時の自動起動はしてくれないらしい
sudo service docker start

sudo docker run hello-world

# https://docs.docker.com/engine/install/linux-postinstall/
sudo groupadd docker
sudo usermod -aG docker $USER

# 自動起動設定
# https://qiita.com/ko-zi/items/949d358163bbbad5a91e
tee -a ~/.bashrc <<"EOS"
# docker
echo $(service docker status | awk '{print $4}') #起動状態を表示
if test $(service docker status | awk '{print $4}') = 'not'; then #停止状態
  sudo /usr/sbin/service docker start #起動
fi
EOS
```


### データベースの設定

### パフォーマンスを向上させるために GPU アクセラレーションを設定する

### 外部ドライブまたは USB をマウントする



