# CentOS8 on Docker

https://hub.docker.com/_/centos

基本的には通常の CentOS 上での作業の練習台として Systemd 利用して起動したものを利用する。

## ベースイメージ

- `Dockerfile`
  ```
  FROM centos:8
  ENV container docker
  RUN (cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == \
  systemd-tmpfiles-setup.service ] || rm -f $i; done); \
  rm -f /lib/systemd/system/multi-user.target.wants/*;\
  rm -f /etc/systemd/system/*.wants/*;\
  rm -f /lib/systemd/system/local-fs.target.wants/*; \
  rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
  rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
  rm -f /lib/systemd/system/basic.target.wants/*;\
  rm -f /lib/systemd/system/anaconda.target.wants/*;
  VOLUME [ "/sys/fs/cgroup" ]
  CMD ["/usr/sbin/init"]
  ```
- ビルド
  ```
  docker build --rm -t local/c8-systemd ./systemd
  ```

## サンプルイメージ

- httpd
  - `Dockerfile`
    ```
    FROM local/c8-systemd
    RUN yum -y install httpd; yum clean all; systemctl enable httpd.service
    EXPOSE 80
    CMD ["/usr/sbin/init"]
    ```
  - ビルド
    ```
    docker build --rm -t local/c8-systemd-httpd ./httpd
    ```
  - 実行
    - ホストが Ubuntu 以外
      ```
      docker run \
        -d --name c8-httpd \
        -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
        -p 80:80 \
        local/c8-systemd-httpd
      ```
    - ホストが Ubuntu の場合は `-v /tmp/$(mktemp -d):/run` が必要
      ```
      docker run \
        -d --name c8-httpd \
        -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
        -v /tmp/$(mktemp -d):/run \
        -p 80:80 \
        local/c8-systemd-httpd
      ```
  - 動作確認
    ```
    curl localhost:80
    ```
- ssh
  - `Dockerfile`
    SSH をまともに使えるようにするための初期設定が未  
    ```
    FROM local/c8-systemd
    RUN yum -y install openssh-server; yum clean all; systemctl enable sshd.service
    EXPOSE 22
    CMD ["/usr/sbin/init"]
    ```
  - ビルド
    ```
    docker build --rm -t local/c8-systemd-sshd ./sshd
    ```
  - 実行
    - ホストが Ubuntu 以外
      ```
      docker run \
        -d --name c8-sshd \
        -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
        -p 10022:22 \
        local/c8-systemd-sshd
      ```
    - ホストが Ubuntu の場合は `-v /tmp/$(mktemp -d):/run` が必要
      ```
      docker run \
        -d --name c8-sshd \
        -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
        -v /tmp/$(mktemp -d):/run \
        -p 10022:22 \
        local/c8-systemd-sshd
      ```
  - 動作確認
    ```
    docker exec -it c8-sshd sed -i -e "s/^#PubkeyAuthentication yes/PubkeyAuthentication yes/g" /etc/ssh/sshd_config \
    && docker exec -it c8-sshd sed -i -e "s/^PasswordAuthentication yes/PasswordAuthentication no/g" /etc/ssh/sshd_config \
    && docker exec -it c8-sshd systemctl reload sshd \
    && docker exec -it c8-sshd mkdir /root/.ssh \
    && docker exec -it c8-sshd chmod 700 /root/.ssh \
    && docker exec -it c8-sshd touch /root/.ssh/authorized_keys \
    && docker exec -it c8-sshd chmod 600 /root/.ssh/authorized_keys \
    && docker exec -it c8-sshd sh -c "echo `cat ~/.ssh/id_rsa.pub` >> /root/.ssh/authorized_keys"

    ssh root@localhost -p 10022 \
      -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null \
      -i ~/.ssh/id_rsa
    ```
